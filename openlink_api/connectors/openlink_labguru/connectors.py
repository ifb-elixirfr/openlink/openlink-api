import base64
import logging
import os.path
import requests

from django.core.files import File
from bs4 import BeautifulSoup
from urllib.parse import urlparse

from openlink_api.core.connector import (
    AuthentificationError,
    DataObject,
    ISAImporter,
    ToolConnector,
    LIST_STRUCTURE
)

logger = logging.getLogger(__name__)


class LabGuruConnector(ISAImporter):

    def __init__(self, parameters):
        self.url = parameters["url"]
        self.private_token = parameters["private_token"]

        self.api_url = f"{self.url}/api/v1"

        test_url = f"{self.api_url}/admin/members.json?token={self.private_token}"
        test = requests.get(test_url)
        if test.status_code != requests.codes.ok:
            if test.status_code == 401:
                raise AuthentificationError("Invalid token")
            else:
                raise AuthentificationError("Unexpected LabGuru connection error")

    #
    # Default fonction
    #
    @classmethod
    def get_name(cls):
        return "LabGuru"

    @classmethod
    def get_description(cls):
        return "Connect to a LabGuru electronic lab notebook"

    @classmethod
    def get_type(cls):
        return ["dataconnector", "isaimporter"]

    @classmethod
    def get_connection_param(cls):
        return [
                {
                    "id": "url",
                    "name": "LabGuru URL",
                    "helper": "Exemple :  https://my.labguru.com",
                    "type": "text",
                    "access": "public",
                },
                {
                    "id": "private_token",
                    "name": "Private token",
                    "helper": "How to get a LabGuru token ?<br/>1. Connect to LabGuru<br/>2. Go to your LabGuru Profile<br/>3. Click on LabGuru's Uplfolder<br/>4. Click on the blue rectangle that says \"Click here to get a token by email\"<br/>5. You will receive your LabGuru token in your mailbox",
                    "type": "text",
                    "access": "private",
                }
            ]

    @classmethod
    def get_data_structure(cls):
        return LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_logo(cls):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        logo = os.path.join(dir_path, "images/labguru.png")
        f = open(logo, 'rb')
        image = File(f)
        data = base64.b64encode(image.read())
        f.close()
        return data

    @classmethod
    def get_color(cls):
        return "#F7F5FF"

    def get_instance_name(self):
        parsed_uri = urlparse(self.api_url)
        return parsed_uri.netloc

    def get_url_link_to_an_object(self, obj_type, obj_id):
        return "https://todo/"+obj_id

    def get_root_name(self):
        return "All projects"

    def get_data_objects(self, parent_type, parent_id):
        datas = []
        if parent_id is None:
            # retrieve project
            url = f"{self.api_url}/projects?token={self.private_token}"
            projects = requests.get(url)

            for project in projects.json():
                datas.append(
                    DataObject(
                        id = project['id'],
                        name = project['name'],
                        description = BeautifulSoup(project['description'], features='html.parser').get_text(),
                        type = 'project',
                        icon = "",
                        has_children = True,
                        linkable = True,
                        children = [],
                    )
                )

        elif parent_type == 'project':
            # retrieve folders
            url = f"{self.api_url}/milestones?token={self.private_token}&meta=true&project_id={parent_id}"
            folders = requests.get(url)

            for folder in folders.json():
                datas.append(
                    DataObject(
                        id = folder["id"],
                        name = folder["name"],
                        type = 'folder',
                        icon = '',
                        has_children = True,
                        linkable = True,
                        children = [],
                    )
                )


        elif parent_type == 'folder':
            # retrive experiments
            url = f"{self.api_url}/experiments?token={self.private_token}&filter={{\"milestone_id\":{parent_id}}}"
            experiments = requests.get(url)

            for experiment in experiments.json():
                datas.append(
                    DataObject(
                        id = experiment["id"],
                        name = experiment["name"],
                        type = "experiment",
                        icon = '',
                        has_children = False,
                        linkable = True,
                        children = [],
                    )
                )

        return sorted(datas, key=lambda data: data.name)

    def get_data_object(self, object_type, object_id, depth=0):

        if object_type == 'project':
            url = f"{self.api_url}/projects/{object_id}?token={self.private_token}"
            project = requests.get(url).json()
            parent = DataObject(
                id = project['id'],
                name = project['name'],
                description = BeautifulSoup(project['description']).get_text(),
                type = 'project',
                icon = "",
                has_children = True,
                linkable = True,
                children = [],
            )
            if int(depth) > 0:
                for child in self.get_data_objects('project', project['id']):
                    parent.children.append(self.get_data_object(child.type, child.id, int(depth)-1))
        elif object_type == 'folder':
            url = f"{self.api_url}/milestones/{object_id}?token={self.private_token}"
            folder = requests.get(url).json()
            parent = DataObject(
                id = folder["id"],
                name = folder["name"],
                description = BeautifulSoup(folder['description']).get_text(),
                type = "folder",
                icon = "",
                has_children = True,
                linkable = True,
                children = [],
            )
            if int(depth) > 0:
                for child in self.get_data_objects('folder', folder['id']):
                    parent.children.append(self.get_data_object(child.type, child.id, int(depth)-1))
        elif object_type == 'experiment':
            url = f"{self.api_url}/experiments/{object_id}?token={self.private_token}"
            experiment = requests.get(url).json()
            parent = DataObject(
                id = experiment["id"],
                name = experiment["name"],
                description = "",
                type = "experiment",
                icon = "",
                has_children = False,
                linkable = True,
                children = [],
            )
        return parent

ToolConnector.register(LabGuruConnector)
