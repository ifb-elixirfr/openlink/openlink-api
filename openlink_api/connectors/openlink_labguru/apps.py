from django.apps import AppConfig


class OpenlinkLabGuruConfig(AppConfig):
    name = "openlink_api.connectors.openlink_labguru"
