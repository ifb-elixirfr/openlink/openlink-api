from django.apps import AppConfig


class OpenlinkOmeroConfig(AppConfig):
    name = "openlink_api.connectors.openlink_omero"
