# from django.shortcuts import render
import logging
import os
from time import sleep
from urllib.parse import urlparse
import omero
from django import forms
from Glacier2 import PermissionDeniedException
from Ice import DNSException
from omero.cli import cli_login
from omero.gateway import BlitzGateway
from openlink_api.core.connector import (
    AuthentificationError,
    DataObject,
    DataObject,
    DataConnector,
    ToolConnector,
    ToolUnreachableError,
    defaultError,
    LIST_STRUCTURE
)
from django.core.files import File
import base64
logger = logging.getLogger(__name__)


class OmeroConnector(DataConnector):
    def __init__(self, parameters):
        self.url = parameters["url"]
        self.login = parameters["login"]
        self.password = parameters["password"]

        try:
            self.get_omero_instance()
        except Exception as e:
            raise e


    @classmethod
    def get_name(cls):
        return "Omero"

    @classmethod
    def get_description(cls):
        return "OMERO handles all your images using a secure central repository, from the microscope to publication."

    @classmethod
    def get_type(cls):
        return "dataconnector"

    @classmethod
    def get_data_structure(cls):
        return LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_logo(cls):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        logo = os.path.join(dir_path, "images/omero.png")
        f = open(logo, 'rb')
        image = File(f)
        data = base64.b64encode(image.read())
        f.close()
        return data

    @classmethod
    def get_color(cls):
        color = "#7CDD98"
        return color

    @classmethod
    def get_connection_param(cls):
        return [
                {
                    "id": "url",
                    "name": "Omero server URL",
                    "helper": "Exemple :  https://omero.mesocentre.uca.fr/",
                    "type": "text",
                    "access": "public",
                },
                {
                    "id": "login",
                    "name": "Private login",
                    "helper": "",
                    "type": "text",
                    "access": "private",
                },
                {
                    "id": "password",
                    "name": "Private password",
                    "helper": "",
                    "type": "password",
                    "access": "private",
                }
            ]

    @classmethod
    def has_mapping_options(cls):
        return False

    def get_port(self):
        return 4064

    def get_host(self):
        url_parse = urlparse(self.url)
        host = url_parse.hostname
        return host

    @classmethod
    def check_status_code(cls, code):
        if type(code) == PermissionDeniedException:
            raise AuthentificationError(cls, "login or password")
        elif type(code) == DNSException:
            raise ToolUnreachableError(cls)
        elif type(code) == omero.ClientError:
            raise ToolUnreachableError(cls)
        else:
            raise defaultError(cls)

    def get_api_url(self):
        WEB_HOST = "v0/"
        url_api = "%s/api/%s" % (self.url, WEB_HOST)
        url_api = url_api.replace("//api", "/api")
        return url_api

    def get_url_link_to_an_object(self, obj_type, obj_id):
        return self.url

    def get_instance_name(self):
        url_parse = urlparse(self.url)
        host = url_parse.hostname
        return "omero:" + str(host)

    def get_root_name(self):
        return "All project"

    def get_data_objects(self, parent_type, parent_id):
        client = self.get_omero_instance()
        datas = []

        if parent_id:
            if parent_type.lower() == "project":
                conn = BlitzGateway(client_obj=client)
                conn.SERVICE_OPTS.setOmeroGroup("-1")
                for data in conn.getObjects("Dataset", opts={"project": parent_id}):
                    datas.append(
                        DataObject(
                            id=data.getId(),
                            name=data.getName(),
                            type="dataset",
                            description=data.getDescription(),
                            has_children=True if data.countChildren() else False,
                            linkable=True,
                            icon="file"
                        )
                    )
            if parent_type.lower() == "dataset":
                conn = BlitzGateway(client_obj=client)
                conn.SERVICE_OPTS.setOmeroGroup("-1")
                for data in conn.getObjects("Image", opts={"dataset": parent_id}):
                    datas.append(
                        DataObject(
                            id=data.getId(),
                            name=data.getName(),
                            type="image",
                            description=data.getDescription(),
                            has_children=False,
                            linkable=True,
                            icon="image"
                        )
                    )
        else:
            conn = BlitzGateway(client_obj=client)
            conn.SERVICE_OPTS.setOmeroGroup("-1")
            for data in conn.listProjects():
                datas.append(
                    DataObject(
                        id=data.getId(),
                        name=data.getName(),
                        type="project",
                        description=data.getDescription(),
                        has_children=True if data.countChildren() else False,
                        linkable=True,
                        icon="folder"
                    )
                )
        return datas

    def get_data_object(self, object_type, object_id):
        client = self.get_omero_instance()
        if object_id:
            if object_type.lower() == "project":
                conn = BlitzGateway(client_obj=client)
                conn.SERVICE_OPTS.setOmeroGroup("-1")
                object = conn.getObject("project", object_id)
                children = True if object.countChildren() else False
                icon = "folder"

            if object_type.lower() == "dataset":
                conn = BlitzGateway(client_obj=client)
                conn.SERVICE_OPTS.setOmeroGroup("-1")
                object = conn.getObject("dataset", object_id)
                children = True if object.countChildren() else False
                icon = "file"

            if object_type.lower() == "image":
                conn = BlitzGateway(client_obj=client)
                conn.SERVICE_OPTS.setOmeroGroup("-1")
                object = conn.getObject("image", object_id)
                children = False
                icon = "image"

            data = DataObject(
                id=object.getId(),
                name=object.getName(),
                type=object_type,
                description=object.getDescription(),
                has_children=children,
                icon=icon,
                url=self.get_url_link_to_an_object(object_type, object.getId()),
            )
        else:
            return None
        return data

    def download(self, object_id, target_dir):
        with cli_login(
            "%s@%s:%s" % (str(self.login), str(self.get_host()), str(self.get_port())),
            "-w",
            str(self.password),
        ) as cli:
            conn = BlitzGateway(client_obj=cli._client)
            conn.SERVICE_OPTS.setOmeroGroup("-1")
            # parent = conn.getObject("Dataset", object_id)
            # data_dir = os.path.join(target_dir, parent.name)
            os.makedirs(target_dir, exist_ok=True)
            image = conn.getObject("Image", object_id)
            # image_dir = os.path.join(target_dir, image.name)
            cli.invoke(["download", f"Image:{image.getId()}", target_dir])
            # for image in parent.listChildren():
            #     image_dir = os.path.join(data_dir, image.name)
            #     cli.invoke(["download", f"Image:{image.id}", image_dir])
        return None

    def check_file_access(self, object_id):
        with cli_login(
            "%s@%s:%s" % (str(self.login), str(self.get_host()), str(self.get_port())),
            "-w",
            str(self.password),
        ) as cli:
            conn = BlitzGateway(client_obj=cli._client)
            conn.SERVICE_OPTS.setOmeroGroup("-1")
            try:
                conn.getObject("Image", object_id)
                access = True
            except Exception as e:
                logging.debug(e)
                access = False
        return access

    def get_omero_instance(self):
        client = omero.client(self.get_host(), self.get_port())
        try:
            client.createSession(str(self.login), str(self.password))
        except PermissionDeniedException:
            raise forms.ValidationError("Invalid Omero login or password")
        except omero.ClientError:
            raise forms.ValidationError("Unable to connect to Omero host")
        except DNSException:
            raise forms.ValidationError("Unable to connect to Omero host")
        return client

ToolConnector.register(OmeroConnector)
