import base64
import logging
import os.path
import stat

from django.core.files import File
from paramiko.client import AutoAddPolicy, SSHClient
from paramiko.ssh_exception import AuthenticationException, NoValidConnectionsError, SSHException

from openlink_api.core.connector import (
    AuthentificationError,
    DataObject,
    DataConnector,
    ToolConnector,
    LIST_STRUCTURE,
)

logger = logging.getLogger(__name__)


class SSHFSConnector(DataConnector):

    def __init__(self, parameters):
        self.host = parameters["host"]
        self.username = parameters["username"]
        self.password = parameters["password"]

        self.client = SSHClient()
        self.client.set_missing_host_key_policy(AutoAddPolicy)
        try:
            self.client.connect(hostname=self.host, username=self.username, password=self.password)
        except AuthenticationException:
            raise AuthentificationError("Invalid username or password")
        except NoValidConnectionsError:
            raise AuthentificationError("Unable to establish connection to host")
        except SSHException:
            raise AuthentificationError("Unexpected SSH connection error")
        self.sftp = self.client.open_sftp()
    #
    # Default fonction
    #

    @classmethod
    def get_name(cls):
        return "SSHFS"

    @classmethod
    def get_description(cls):
        return "Connect to any NAS or cluster through SSH"

    @classmethod
    def get_type(cls):
        return "dataconnector"

    @classmethod
    def get_connection_param(cls):
        return [
                {
                    "id": "host",
                    "name": "SSH host",
                    "helper": "Exemple :  core.cluster.france-bioinformatique.fr",
                    "type": "text",
                    "access": "public",
                },
                {
                    "id": "username",
                    "name": "Username",
                    "helper": "",
                    "type": "text",
                    "access": "private",
                },
                {
                    "id": "password",
                    "name": "Password",
                    "helper": "",
                    "type": "password",
                    "access": "private",
                }
            ]

    @classmethod
    def get_data_structure(cls):
        return LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return False

    @classmethod
    def get_logo(cls):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        logo = os.path.join(dir_path, "images/server-network.png")
        f = open(logo, 'rb')
        image = File(f)
        data = base64.b64encode(image.read())
        f.close()
        return data

    @classmethod
    def get_color(cls):
        return "#F5FEFF"

    def get_instance_name(self):
        return self.host

    def get_url_link_to_an_object(self, obj_type, obj_id):
        return "todo"

    def get_root_name(self):
        return "/"

    def get_data_objects(self, parent_type, parent_id):
        datas = []
        path = '/'

        if parent_type is not None:
            path = parent_type

        try:
            for file_attr in self.sftp.listdir_iter(path):
                is_dir = stat.S_ISDIR(file_attr.st_mode)

                datas.append(DataObject(
                    id = os.path.join(path, file_attr.filename),
                    name = file_attr.filename,
                    type = 'directory' if is_dir else 'file',
                    icon = "",
                    has_children = is_dir,
                    linkable = True
                ))
        except Exception:
            pass

        return sorted(datas, key=lambda data: data.name)

    def get_data_object(self, object_type, object_id):
        return DataObject(
            id = object_id,
            name = os.path.basename(object_id),
            type = object_type,
            icon = "",
            has_children = object_type == 'directory',
            linkable = True
        )


ToolConnector.register(SSHFSConnector)
