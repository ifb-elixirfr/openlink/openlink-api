from django.apps import AppConfig


class OpenlinkSSHFSConfig(AppConfig):
    name = "openlink_api.connectors.openlink_sshfs"
