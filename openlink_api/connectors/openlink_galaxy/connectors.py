import logging
import os
import openlink_api.core.connector
import requests
from bioblend import galaxy
from openlink_api.core.connector import (
    AuthentificationError,
    DataObject,
    DataConnector,
    ToolConnector,
    defaultError,
    LIST_STRUCTURE,
)
from django.core.files import File
import base64
from urllib.parse import urlparse
logger = logging.getLogger(__name__)


class GalaxyConnector(DataConnector):
    def __init__(self, parameters):
        self.url = parameters["url"]
        self.login = parameters["login"]
        self.password = parameters["password"]

        try:
            self.get_galaxy_instance()
        except Exception as e:
            raise e

    #
    # Default fonction
    #

    @classmethod
    def get_name(cls):
        return "Galaxy"

    @classmethod
    def get_description(cls):
        return "Galaxy is an open source, web-based platform for data intensive biomedical research."

    @classmethod
    def get_type(cls):
        return "dataconnector"

    @classmethod
    def get_connection_param(cls):
        return [
                {
                    "id": "url",
                    "name": "Galaxy server URL",
                    "helper": "Exemple :  https://usegalaxy.org/",
                    "type": "text",
                    "access": "public",
                },
                {
                    "id": "login",
                    "name": "Private login",
                    "helper": "",
                    "type": "text",
                    "access": "private",
                },
                {
                    "id": "password",
                    "name": "Private password",
                    "helper": "",
                    "type": "password",
                    "access": "private",
                }
            ]

    @classmethod
    def get_data_structure(cls):
        return LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_logo(cls):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        logo = os.path.join(dir_path, "images/logo-galaxy.png")
        f = open(logo, 'rb')
        image = File(f)
        data = base64.b64encode(image.read())
        f.close()
        return data

    @classmethod
    def get_color(cls):
        color = "#7FAEE5"
        return color

    @classmethod
    def check_status_code(cls, r):
        if r != requests.codes.ok:
            if r == 401:
                raise AuthentificationError(cls, "login or password")
            else:
                raise defaultError(cls)

    def get_instance_name(self):
        gi = galaxy.GalaxyInstance(url=self.url, email=self.login, password=self.password, verify=True)
        name = gi.config.get_config()["brand"]
        if name:
            return "Galaxy " + str(name)
        else:
            url_parse = urlparse(gi.base_url)
            host = url_parse.hostname
            return "Galaxy:" + str(host)

    def get_url_link_to_an_object(self, object_type, object_id):
        if object_type == "history":
            url = os.path.join(self.url + f"history/switch_to_history?hist_id={object_id}")
        elif object_type == "data":
            url = self.url
        return url

    def get_root_name(self):
        return "All histories"

    def get_data_objects(self, parent_type, parent_id):
        if parent_id:
            if parent_type == "history":
                datas = []
                gi = self.get_galaxy_instance()
                di = gi.datasets.get_datasets(history_id=parent_id)
                if di:
                    for d in di:
                        if not d["deleted"]:
                            ds = gi.datasets.show_dataset(dataset_id=str(d["id"]))
                            size = ds["file_size"]
                            datas.append(
                                DataObject(
                                    id=d["id"],
                                    name=d["name"],
                                    type="dataset",
                                    url=self.get_url_link_to_an_object("data", d["id"]),
                                    size=int(size),
                                    icon="file",
                                    has_children=False,
                                    linkable=True
                                )
                            )
                    return datas
                else:
                    raise Exception("Parent_id error")
            else:
                raise Exception("Parent_type error")
        else:
            gi = self.get_galaxy_instance()
            histories = []
            hi = gi.histories.get_histories()
            for h in hi:
                histories.append(
                    DataObject(
                        id=h["id"],
                        name=h["name"],
                        type="history",
                        icon="folder",
                        url=self.get_url_link_to_an_object("history", h["id"]),
                        has_children=not gi.histories.show_history(history_id=h["id"])["empty"],
                        linkable=True
                    )
                )
            return histories

    def get_data_object(self, object_type, object_id, depth=0):
        gi = self.get_galaxy_instance()
        if object_id:
            if object_type.lower() == "history":
                object = gi.histories.show_history(history_id=object_id)
                data = DataObject(
                    id=object["id"],
                    name=object["name"],
                    type="history",
                    icon="folder",
                    url=self.get_url_link_to_an_object("history", object["id"]),
                    linkable=True,
                    )
                if int(depth) > 0:
                    children = self.get_data_objects('history', object['id'])
                    if children:
                        data.has_children=True
                        data.children=[]
                        for child in children:
                            data.children.append(self.get_data_object(child.type, child.id, int(depth)-1))
            if object_type.lower() == "dataset":
                object = gi.datasets.show_dataset(dataset_id=object_id)
                data = DataObject(
                    id=object["id"],
                    name=object["name"],
                    type="data",
                    icon="file",
                    url=self.get_url_link_to_an_object("data", object["id"]),
                    size=int(object["file_size"]),
                    linkable=True,
                    )
        else:
            return None
        return data

    def download(self, object_id, path):
        gi = self.get_galaxy_instance()
        try:
            di = gi.datasets.show_dataset(dataset_id=object_id.strip())
            name = di["name"]
            gi.datasets.download_dataset(
                dataset_id=object_id.strip(),
                file_path=path + "/" + name,
                use_default_filename=False,
                require_ok_state=False,
            )
        except Exception as e:
            raise e

    def check_file_access(self, object_id):
        try:
            gi = self.get_galaxy_instance()
        except Exception as e:
            logger.debug(e)
            access = False
        try:
            gi.datasets.show_dataset(dataset_id=object_id.strip())
            access = True
        except Exception as e:
            logger.debug(e)
            access = False
        return access

    def get_galaxy_instance(self):
        try:
            gi = galaxy.GalaxyInstance(
                url=self.url, email=self.login, password=self.password, verify=True
            )
            self.check_status_code(gi.make_get_request(url=gi.base_url).status_code)
        except Exception as e:
            raise e
        return gi


ToolConnector.register(GalaxyConnector)
