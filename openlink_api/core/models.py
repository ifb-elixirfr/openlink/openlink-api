import logging
import os
import hvac
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

logger = logging.getLogger(__name__)

vault_url = settings.OPENLINK_VAULT_URL

class OpenlinkUser(AbstractUser):
    vault_id = models.CharField(max_length=100, blank=True)
    
    def __str__(self):
        return str(self.username)


class Token(models.Model):
    vault_token = models.CharField(primary_key=True, max_length=1000)
    token_key = models.CharField(max_length=1000, blank=True, null=True)


class Tool(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    connector = models.CharField(max_length=100, blank=True, null=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    date_created = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    def get_all_public_param(self):
        return ToolParam.objects.filter(tool=self.id)

    def get_public_param(self, key):
        value = ToolParam.objects.values_list("value", flat=True).get(
            key=key, tool_id=str(self.id)
        )
        return value

    def get_private_param(self, key, token):
        if token is not None:
            try:
                vault_client = hvac.Client(url=vault_url, token=token)
                vault_client.kv.default_kv_version = 1
                reponse = vault_client.lookup_token()
                path = os.path.join(reponse["data"]["entity_id"], str(self.id))
                value = vault_client.secrets.kv.v1.read_secret(
                    mount_point="openlink", path=path
                )
                return value["data"][key]
            except Exception as e:
                raise e
        else:
            return None


class ToolParam(models.Model):
    tool = models.ForeignKey(Tool, on_delete=models.CASCADE, null=True)
    key = models.CharField(max_length=100, blank=True, null=True)
    value = models.CharField(max_length=100, blank=True, null=True)


class DataLink(models.Model):
    name = models.CharField(max_length=1000, blank=True)
    tool = models.ForeignKey(Tool, on_delete=models.CASCADE, blank=True, null=True)
    dataobject_id = models.CharField(max_length=1000, blank=True, null=True)
    dataobject_type = models.CharField(max_length=100, blank=True, null=True)
    remote_url = models.CharField(max_length=1000, blank=True, null=True)
    size = models.BigIntegerField(blank=True, null=True)
    icon = models.CharField(max_length=10000, blank=True, null=True)
    has_children = models.BooleanField(default=False)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    isaobject = GenericForeignKey()
    
    class Meta:
        indexes = [
            models.Index(fields=["content_type", "object_id"]),
        ]


class ISAObject(models.Model):
    ISA_STATUS = (
        ('pending', 'pending'),
        ('active', 'active'),
        ('archived', 'archived'),
    )
    name = models.CharField(max_length=1000, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    status = models.CharField(max_length=100, blank=False, choices=ISA_STATUS)
    date_created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    date_modified = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True


class Investigation(ISAObject):
    datalinks = GenericRelation(DataLink, related_query_name='investigation')


class Study(ISAObject):
    datalinks = GenericRelation(DataLink, related_query_name='study')
    parent_investigation = models.ForeignKey(
        Investigation,
        related_name='children',
        on_delete=models.CASCADE,
        blank=True,
        null=True)


class Assay(ISAObject):
    datalinks = GenericRelation(DataLink, related_query_name='assay')
    parent_study = models.ForeignKey(
        Study,
        related_name='children',
        on_delete=models.CASCADE,
        blank=True, 
        null=True)
