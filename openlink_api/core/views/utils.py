import hvac
import os
from django.conf import settings
from openlink_api.core.views.tokens import get_vault_token


def write_secret_in_vault(request, tool_id, data):
    """register secret in vault"""
    vault_client = hvac.Client(
        url=settings.OPENLINK_VAULT_URL, token=get_vault_token(request.auth)
    )
    reponse = vault_client.lookup_token()
    path = os.path.join(reponse["data"]["entity_id"], str(tool_id))
    vault_client.secrets.kv.v1.create_or_update_secret(
        mount_point="openlink", path=path, secret=data
    )


def delete_secret_from_vault(request, tool_id):
    """delete secret in vault"""
    vault_client = hvac.Client(
        url=settings.OPENLINK_VAULT_URL, token=get_vault_token(request.auth)
    )
    reponse = vault_client.lookup_token()
    path = os.path.join(reponse["data"]["entity_id"], str(tool_id))
    vault_client.secrets.kv.v1.delete_secret(mount_point="openlink", path=path)