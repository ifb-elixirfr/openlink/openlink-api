import logging
from openlink_api.core.serializers import connectors
from openlink_api.core import models
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from openlink_api.core import connector as tool
from openlink_api.core.views.permissions import IsOwner
from rest_framework import permissions
from django.shortcuts import get_object_or_404
from openlink_api.core import connector as conn
from openlink_api.core.views.utils import write_secret_in_vault, delete_secret_from_vault

logger = logging.getLogger(__name__)


class ToolsListView(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = connectors.ToolSerializer

    def get(self, request):
        """return a list of tools"""
        connector_type = self.request.query_params.get("type")
        tool_list = []
        tools = models.Tool.objects.filter(owner=request.user)
        if connector_type:
            for tool in tools:
                connector_class = conn.get_connector_class(tool.connector)
                if connector_type == connector_class.get_type() or connector_type in connector_class.get_type():
                    tool_list.append(tool)
        else:
            tool_list = tools

        data_list = []
        for tool in tool_list:
            data = connectors.ToolSerializer(tool).data
            data["parameters"] = {}
            tool_param_serializer = connectors.ToolParamSerializer(tool.get_all_public_param(), many=True)
            for param in tool_param_serializer.data:
                data["parameters"].update({param["key"]: param["value"]})
            data_list.append(data)
        return Response(data_list)

    def post(self, request, format=None):
        """create a tool"""
        try:
            tool_data = {"name": request.data["name"], "connector": request.data["connector"]}
            tool_param_data = request.data["parameters"]
            connector_class = tool.get_connector_class(tool_data["connector"])
            connector = connector_class(tool_param_data)
        except Exception as e:
            if type(e) == KeyError:
                return Response("Field "+str(e)+" not found", status=status.HTTP_400_BAD_REQUEST)
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)

        if not tool_data["name"]:
            tool_data["name"] = connector.get_instance_name()
        serializer_tool = connectors.ToolSerializer(data=tool_data)
        if serializer_tool.is_valid():
            new_tool = serializer_tool.save(owner=request.user)

            public_param = {k["id"]: tool_param_data[k["id"]] for k in connector.get_connection_param() if k["access"] == "public"}
            private_param = {k["id"]: tool_param_data[k["id"]] for k in connector.get_connection_param() if k["access"] == "private"}
            for key, value in public_param.items():
                serializer_toolparam = connectors.ToolParamSerializer(data={"key": key, "value": value})
                if serializer_toolparam.is_valid():
                    serializer_toolparam.save(tool=new_tool)
                else:
                    del connector
                    return Response(serializer_toolparam.errors, status=status.HTTP_400_BAD_REQUEST)
            write_secret_in_vault(request, new_tool.id, private_param)
        else:
            del connector
            return Response(serializer_tool.errors, status=status.HTTP_400_BAD_REQUEST)
        data = serializer_tool.data
        data["parameters"] = {}
        tool_param_serializer = connectors.ToolParamSerializer(new_tool.get_all_public_param(), many=True)
        for param in tool_param_serializer.data:
            data["parameters"].update({param["key"]: param["value"]})
        del connector
        return Response(data, status=status.HTTP_201_CREATED)


class ToolView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsOwner]
    serializer_class = connectors.ToolSerializer

    def get(self, request, *args, **kwargs):
        """return the specified tool"""
        tool = get_object_or_404(models.Tool, pk=kwargs["tid"])
        self.check_object_permissions(self.request, tool)
        serializer = connectors.ToolSerializer(tool)
        tool_param_serializer = connectors.ToolParamSerializer(tool.get_all_public_param(), many=True)
        data = serializer.data
        data["parameters"] = {}
        for param in tool_param_serializer.data:
            data["parameters"].update({param["key"]: param["value"]})
        return Response(data)

    def put(self, request, *args, **kwargs):
        """modify parameters for the specified tool"""
        raise NotImplementedError()

    def delete(self, request, *args, **kwargs):
        """delete the specified tool"""
        tool = get_object_or_404(models.Tool, pk=kwargs["tid"])
        self.check_object_permissions(self.request, tool)
        delete_secret_from_vault(request, tool.id)
        tool.delete()
        return Response("Succesfuly deleted", status=status.HTTP_204_NO_CONTENT)


class ToolObjectsView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsOwner]
    serializer_class = connectors.ToolSerializer

    def get(self, request, *args, **kwargs):
        """retrieve a list of objets from the specified tool"""
        tool = get_object_or_404(models.Tool, pk=kwargs["tid"])
        self.check_object_permissions(self.request, tool)
        connector = conn.set_connector(conn, tool, request.auth)
        mapping_object_list = {"root_name": connector.get_root_name(), "objects": []}
        depth = self.request.query_params.get("depth")
        if depth:
            try:
                data_object = connector.get_data_object(self.request.query_params.get("parent_type"), self.request.query_params.get("parent_id"), depth)
            except Exception as e:
                print(e)
                return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
            data_objects = [data_object]
        else:
            try:
                data_objects = connector.get_data_objects(self.request.query_params.get("parent_type"), self.request.query_params.get("parent_id"))
            except Exception as e:
                return Response(str(e), status=status.HTTP_400_BAD_REQUEST)

        for data in data_objects:
            serializer = connectors.DataObjectSerializer(data)
            mapping_object_list["objects"].append(serializer.data)
            
            
        return Response(mapping_object_list)
