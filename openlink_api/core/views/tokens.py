import logging
import base64
import hvac
from hvac.exceptions import InvalidRequest

from django.conf import settings
from django.contrib.auth.signals import user_logged_out
from django.contrib.auth import get_user_model
User = get_user_model()

from openlink_api.core.serializers.user import UserSerializer, LDAPLoginSerializer
from openlink_api.core.models import Token 
from rest_framework import status
from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import NotFound
from knox.auth import TokenAuthentication
from knox.models import AuthToken
from knox.settings import CONSTANTS
from py_jwt_verifier import PyJwtVerifier, PyJwtException

logger = logging.getLogger(__name__)


def get_vault_token(auth_token):
    try:
        tokens = Token.objects.get(token_key=auth_token.token_key)
        return tokens.vault_token
    except:
        raise NotFound(detail={"Internal Error": "error with authentication credential"})


class UserAPI(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = UserSerializer

    def get_user(self):
        return self.request.user


def get_credentials_from_base64(request):
    if 'HTTP_AUTHORIZATION' in request.META:
        auth = request.META['HTTP_AUTHORIZATION'].split()
        if len(auth) == 2:

            if auth[0].lower() == "basic":
                data = base64.b64decode(auth[1])
                username, password = data.decode('utf-8').split(':')
                return {"username": username, "password": password}


class TokensView(generics.GenericAPIView):
    authentication_classes = []

    def post(self, request, *args, **kwargs):

        user = None
        vault_token = None
        vault_id = None

        if settings.OPENLINK_AUTH_BACKEND == "LDAP":
            data = get_credentials_from_base64(request)
            serializer = LDAPLoginSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            user = serializer.validated_data
            if user and user.is_active:
                # check vault login
                try:
                    client = hvac.Client(url=settings.OPENLINK_VAULT_URL)
                    response = client.auth.ldap.login(
                        username=data["username"],
                        password=data["password"],
                        mount_point=settings.OPENLINK_VAULT_AUTH_PATH
                    )
                    vault_token = response["auth"]["client_token"]
                    vault_id = response["auth"]["entity_id"]

                except:
                    return Response({"Vault Error": "The provided Login and Password has been rejected or vault my be sealed"})
            else:
                return Response({"Login Error": "The provided Login and Password has been rejected"})

        elif settings.OPENLINK_AUTH_BACKEND == "JWT":

            # retrieve JWT from authorization header
            authorization_header = request.headers.get('Authorization')
            authorization_parts = authorization_header.split(' ')
            if len(authorization_parts) != 2 or authorization_parts[0] != 'Token':
                return Response({"Authentication error": "invalid authorization header"})

            # check JWT
            jwt = authorization_parts[1]
            validator = PyJwtVerifier(jwt, auto_verify=False, cache_enabled=False)
            try:
                payload = validator.verify(True)
            except PyJwtException as e:
                raise e

            # retrieve or create associated user
            try:
                user = User.objects.get(email=payload["payload"]["email"])
            except User.DoesNotExist:
                user = User(username=payload["payload"]["preferred_username"])
                if "family_name" in payload["payload"]:
                    user.last_name = payload["payload"]["family_name"]
                else:
                    user.last_name = payload["payload"]["name"].split(" ")[-1]
                if "given_name" in payload["payload"]:
                    user.first_name = payload["payload"]["given_name"]
                else:
                    user.first_name = payload["payload"]["name"].split(" ")[0]
                user.email = payload["payload"]["email"]
                user.is_staff = False
                user.is_superuser = False
                user.save()

            # authenticates against Vault with JWT
            vault_client = hvac.Client(url=settings.OPENLINK_VAULT_URL)
            try:
                vault_response = vault_client.auth.jwt.jwt_login(
                    role=settings.OPENLINK_VAULT_AUTH_ROLE,
                    jwt=jwt,
                    path=settings.OPENLINK_VAULT_AUTH_PATH)
                vault_token = vault_response["auth"]["client_token"]
                vault_id = vault_response["auth"]["entity_id"]
            except InvalidRequest as e:
                return Response({"Authentication error": "The provided JWT has been rejected "})
        else:
            return Response({"Auth Error": "Auth type is not define in openlink configuration"})

        if vault_token and vault_id and user:
            user.vault_id = vault_id
            user.save()
            knox_instance, knox_token = AuthToken.objects.create(user=user)
            Token.objects.create(vault_token=vault_token, token_key=knox_token[:CONSTANTS.TOKEN_KEY_LENGTH])
            return Response({
                "expiry": knox_instance.expiry,
                "token": knox_token,
                "user": UserSerializer(user, context=self.get_serializer_context()).data,

            })
        else:
            return Response({"Auth Error": "Authentication error"})


class LogoutAPI(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        if "vault_token" in request.session:
            try:
                vault_client = hvac.Client(url=settings.OPENLINK_VAULT_URL, token=request.session['vault_token'])
                vault_client.logout(revoke_token=True)
                print("vault token revoked")
            except Exception as e:
                print("couldn't revoke vault  token")
                print(e)
        request.session.flush()
        request._auth.delete()
        user_logged_out.send(sender=request.user.__class__,
                             request=request, user=request.user)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
