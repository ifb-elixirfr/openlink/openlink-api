from openlink_api.core.serializers import isa
from openlink_api.core.serializers.connectors import DatalinkSerializer
from openlink_api.core import models
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from openlink_api.core.views.permissions import IsOwner
from rest_framework import permissions
from django.shortcuts import get_object_or_404
import json
from openlink_api.core import connector as conn


class InvestigationsListView(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = isa.InvestigationSerializer

    def get(self, request):
        """return a list of investigations"""
        investigations = models.Investigation.objects.filter(owner=request.user)
        status = self.request.query_params.get('status')
        if status:
            investigations = investigations.filter(status=status)
        investigations_serialized = isa.InvestigationReadSerializer(investigations, many=True)
        return Response(investigations_serialized.data)

    def post(self, request, format=None):
        """create an investigation"""
        serializer = isa.InvestigationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(owner=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class InvestigationView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsOwner]
    serializer_class = isa.InvestigationSerializer

    def get(self, request, *args, **kwargs):
        """return the specified investigation"""
        investigation = get_object_or_404(isa.Investigation, pk=kwargs["iid"])
        self.check_object_permissions(self.request, investigation)
        serializer = isa.InvestigationReadSerializer(investigation)
        return Response(serializer.data)

    def put(self, request, *args, **kwargs):
        """
        Modify the specified investigation
        It can be used for datalink and children creation ("import_from")
        """
        investigation = get_object_or_404(isa.Investigation, pk=kwargs["iid"])
        self.check_object_permissions(self.request, investigation)
        if "name" not in request.data and investigation.name is not None:
            request.data["name"] = investigation.name
        serializer = isa.InvestigationSerializer(investigation, data=request.data)
        if self.request.query_params.get("import_from"):
            dry_run = False
            if self.request.query_params.get("dry_run"):
                dry_run = eval(self.request.query_params.get("dry_run").capitalize())
            import_from = self.request.query_params.get("import_from")
            res = json.loads(import_from)
            try:
                tool_id = int(res["tool_id"])
                dataobject_id = res["dataobject_id"]
                dataobject_type = res["dataobject_type"]
                tool = models.Tool.objects.get(pk=tool_id)
                self.check_object_permissions(self.request, tool)
            except Exception as e:
                return Response("Error Parameters: " + str(e), status=status.HTTP_400_BAD_REQUEST)
            connector = conn.set_connector(conn, tool, request.auth)
            data_object = connector.get_data_object(dataobject_type, dataobject_id, depth=2)
            datalinkserializer = DatalinkSerializer.from_dataobject(dataobject=data_object, tool=tool, owner=request.user)
            if datalinkserializer.is_valid():
                datalink_investigation = datalinkserializer.data
            studies =[]
            for data_object_study in data_object.children:
                assays = []
                for data_object_assay in data_object_study.children:
                    datalink_assay_serializer = DatalinkSerializer.from_dataobject(dataobject=data_object_assay, tool=tool, owner=request.user)
                    if datalink_assay_serializer.is_valid():
                        datalink_assay = datalink_assay_serializer.data
                    data_assay = {
                    "name": data_object_assay.name,
                    "description": data_object_assay.description,
                    "status": "active",
                    "owner": request.user.pk,
                    "datalinks": [datalink_assay]
                    }
                    assay_serializer = isa.AssaySerializer(data=data_assay)
                    if assay_serializer.is_valid():
                        assays.append(assay_serializer.data)
                    else:
                        return Response(assay_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                datalink_study_serializer = DatalinkSerializer.from_dataobject(dataobject=data_object_study, tool=tool, owner=request.user)
                if datalink_study_serializer.is_valid():
                    datalink_study = datalink_study_serializer.data
                data_study = {
                    "name": data_object_study.name,
                    "description": data_object_study.description,
                    "status": "active",
                    "datalinks": [datalink_study],
                    "owner": request.user.pk,
                    "children": assays,
                }
                study_serializer = isa.StudySerializer(data=data_study)
                if study_serializer.is_valid():
                    studies.append(study_serializer.data)
                else:
                    return Response(study_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            request.data["name"] = data_object.name
            request.data["description"] = data_object.description
            request.data["status"] = "active"
            request.data["datalinks"] = [datalink_investigation]
            request.data["children"] = studies
            investigation_serializer = isa.InvestigationSerializer(investigation, data = request.data)
            if investigation_serializer.is_valid(raise_exception=True):
                if dry_run == False:
                    investigation_serializer.save(owner=request.user)
                    investigation_serializer = isa.InvestigationSerializer(instance=investigation)
                    return Response(investigation_serializer.data)
                else:
                    return Response(request.data)
            else:
                return Response(investigation_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        """delete the specified investigation"""
        investigation = get_object_or_404(isa.Investigation, pk=kwargs["iid"])
        self.check_object_permissions(self.request, investigation)
        investigation.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
