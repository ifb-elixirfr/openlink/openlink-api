from django.urls import include, path

from openlink_api.core.views import tokens, investigations, studies, assays, connectors, tools, datalinks


assays_url_patterns = [
    # base-path = api/investigations/<iid>/studies/assays/
    path("<int:aid>/", assays.AssayView.as_view()),
]

studies_url_patterns = [
    # base-path = api/investigations/<iid>/studies/
    path("<int:sid>/", studies.StudyView.as_view()),
    path("<int:sid>/assays/", assays.AssaysListView.as_view()),
]

investigation_url_patterns = [
    # base-path = api/investigations/
    path("", investigations.InvestigationsListView.as_view()),
    path("<int:iid>/", investigations.InvestigationView.as_view()),
    path("<int:iid>/studies/", studies.StudiesListView.as_view()),
]

urlpatterns = [
    # base-path = api/
    path('tokens/', tokens.TokensView.as_view()),
    path('user/', tokens.UserAPI.as_view()),
    path('logout/', tokens.LogoutAPI.as_view()),
    path("investigations/", include(investigation_url_patterns)),
    path("studies/", include(studies_url_patterns)),
    path("assays/", include(assays_url_patterns)),

    path('connectors/', connectors.ConnectorsListView.as_view()),
    path('connectors/<str:cid>/', connectors.ConnectorView.as_view()),

    path('tools/', tools.ToolsListView.as_view()), # ?linked=Boolean
    path('tools/<int:tid>/', tools.ToolView.as_view()),

    path('tools/<int:tid>/objects/', tools.ToolObjectsView.as_view()), # ?parent_id=data_object_id&parent_type=data_object_type
    path('datalinks/', datalinks.DatalinksListView.as_view()), # ?parent_id=isa_id&parent_type=isa_type
    path('datalinks/<int:did>/', datalinks.DatalinkView.as_view()),
]
