from django.apps import AppConfig


class ApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'openlink_api.core'
    label = 'core'

    def ready(self):
        from openlink_api.core import signals