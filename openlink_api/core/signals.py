import logging

from django.db.models.signals import pre_delete
from django.dispatch import receiver
from openlink_api.core.models import Token
from knox.models import AuthToken

logger = logging.getLogger(__name__)


@receiver(pre_delete, sender=AuthToken)
def delete__signal(sender, **kwargs):
    authtoken = kwargs["instance"]
    try:
        token_to_delete = Token.objects.get(token_key=authtoken.token_key)
    except Token.DoesNotExist:
        token_to_delete = False
    if token_to_delete:
        token_to_delete.delete()
