from openlink_api.core.models import Investigation, Study, Assay, DataLink
from openlink_api.core.serializers.connectors import DatalinkSerializer
from rest_framework import serializers
from drf_writable_nested.serializers import WritableNestedModelSerializer


class AssaySerializer(WritableNestedModelSerializer):
    datalinks = DatalinkSerializer(many=True, required=False)
    isatype = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Assay
        fields = ('id', 'name', 'description', 'status', 'owner', 'datalinks', 'isatype')
        read_only_fields = ['isatype']

    def get_isatype(self, obj):
        return "Assay"


class AssayReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assay
        fields = ('id', 'name', 'description', 'status', 'owner')


class StudySerializer(WritableNestedModelSerializer):
    datalinks = DatalinkSerializer(many=True, required=False)
    children = AssaySerializer(many=True, required=False)
    isatype = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Study
        fields = ('id', 'name', 'description', 'status', 'owner', 'datalinks', 'children', 'isatype')
        read_only_fields = ['isatype']

    def get_isatype(self, obj):
        return "Study"


class StudyReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Study
        fields = ('id', 'name', 'description', 'status', 'owner')


class InvestigationSerializer(WritableNestedModelSerializer):
    datalinks = DatalinkSerializer(many=True, required=False)
    children = StudySerializer(many=True, required=False)
    isatype = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Investigation
        fields = ('id', 'name', 'description', 'status', 'owner','datalinks', 'children', 'isatype')
        read_only_fields = ['isatype']

    def get_isatype(self, obj):
        return "Investigation"


class InvestigationReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Investigation
        fields = ('id', 'name', 'description', 'status', 'owner')
